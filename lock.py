#!/bin/env python
import requests
import json

def lock_unlock_deployment(operation):
                if operation in ["lock","unlock"]:
                                
                                access_token = authentication()
        
                                request_headers={}
                                request_headers['X-Api-Version'] = "1.5"
                                request_headers['X-Account'] = "84502"
                                request_headers['Authorization'] = "Bearer " + access_token

                                resp = requests.post('https://us-4.rightscale.com/api/deployments/1048131004/' + operation ,headers=request_headers)
                                return "Operation Successful : " + operation
                else:
                                return "Invalid Operation : " + operation
    
def authentication():
                request_payload={}
                request_payload['grant_type'] = "refresh_token"
                request_payload['refresh_token']="2ced6b4b8b81fe9883b31e47c7140234f3081b8d"

                resp = requests.post('https://us-4.rightscale.com/api/oauth2',data=request_payload ,headers={'X-Api-Version':'1.5'})
                content = json.loads(resp.content)
                access_token = content["access_token"]
                return access_token
                
lock_unlock_deployment("lock")